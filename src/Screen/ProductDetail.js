import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";

const ProductDetail = ({ route }) => {
  const { data } = route.params;

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: data.url }} />
      <Text style={styles.title}>{data.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
  },
  title: {
    marginTop: 10,
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    padding: 10,
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: "contain",
  },
});

export default ProductDetail;
