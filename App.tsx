import { View, Text } from "react-native";
import React from "react";
import AppNavigation from "./src/navigator/AppNavigation";

const App = () => {
  return <AppNavigation />;
};

export default App;
